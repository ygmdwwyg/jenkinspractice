﻿// C# example.
using UnityEditor;
using System.Diagnostics;
using UnityEngine;

public class ScriptBatch
{
    [MenuItem("Build Tools/Build APK")]
    public static void BuildAndroid()
    {
        PlayerSettings.Android.bundleVersionCode = 1;
        // Get filename.
        string path = EditorUtility.SaveFolderPanel("Game Build", "C:/Users/h3902340/Desktop/Game Build", "");
        string[] levels = new string[] { "Assets/Scenes/main.unity"};

        // Build player.
        BuildPipeline.BuildPlayer(levels, path + "/Game.apk", BuildTarget.Android, BuildOptions.None);
    }
}